//
//  API.swift
//  Digital Filmes
//
//  Created by Paola Geremia on 08/11/21.
//

import Foundation
import Alamofire

private let baseUrl = "https://api.themoviedb.org/3/movie/"
let posterUrl = "https://image.tmdb.org/t/p/original"
let apiKey = "02a7164f9e76c25bf29f045bfd593976"
private let coder = JSONDecoder()

class API {
    class func fetchPopularMovies(_ onSuccess: @escaping (Results) -> Void) {
        coder.keyDecodingStrategy = .convertFromSnakeCase
        let urlStr = "\(baseUrl)popular?api_key=\(apiKey)"
        guard let url = URL(string: urlStr) else { fatalError("Unable to get url")}
        AF.request(url).response { response in
            switch response.result {
            case .success(let data):
                guard let data = data else { fatalError("Unable to perse data from api")}
                guard let results = try? coder.decode(Results.self, from: data) else { fatalError("Unable to perse data in to json")}
                DispatchQueue.main.async {
                    onSuccess(results)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
