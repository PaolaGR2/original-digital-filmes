//
//  signUpViewController.swift
//  Digital-Filmes-Oficial
//
//  Created by Mariana Eri Massaki on 17/11/21.
//

import Foundation
import UIKit
import Firebase

class signUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var nometextField: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var dataTextField: UITextField!
    @IBOutlet weak var senhaLabel: UILabel!
    @IBOutlet weak var senhaTextField: UITextField!
    
    var auth: Auth?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.auth = Auth.auth()
        
        self.nometextField.delegate = self
        self.emailTextField.delegate = self
        self.dataTextField.delegate = self
        self.senhaTextField.delegate = self
    }
    
    func alert(title:String, message:String) {
        
        let alertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok:UIAlertAction = UIAlertAction(title: title, style: .cancel, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func criarContaButton(_ sender: UIButton) {
        
        let email:String = self.emailTextField.text ?? ""
        let senha:String = self.senhaTextField.text ?? ""
        
        
        self.auth?.createUser(withEmail: email, password: senha, completion: { (result, error) in
            
            if error != nil{
                self.alert(title: "Atenção", message: "Falha ao cadastrar")
            } else{
                self.alert(title: "Parabéns", message: "Sucesso ao cadastrar!!")
            }
            
            
        })
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == self.nometextField {
                self.emailTextField.becomeFirstResponder()
            }
            if textField == self.emailTextField {
                self.dataTextField.becomeFirstResponder()
            }
                else {
                self.senhaTextField.resignFirstResponder()
            }
            return true
        }
    }
    
}
