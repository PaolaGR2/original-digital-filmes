//
//  LoginViewController.swift
//  Digital-Filmes-Oficial
//
//  Created by Mariana Eri Massaki on 17/11/21.
//

import Foundation
import UIKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var emailTexteField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var auth:Auth?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.auth = Auth.auth()
        self.emailTexteField.delegate = self
        self.passwordTextField.delegate = self
    }
    
    func alert(title:String, message:String) {
        
        let alertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok:UIAlertAction = UIAlertAction(title: title, style: .cancel, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func clickEnterButton(_ sender: UIButton) {
        
        let email:String = self.emailTexteField.text ?? ""
        let senha:String = self.passwordTextField.text ?? ""
        
        self.auth?.signIn(withEmail: email, password: senha, completion: { (usuario, error) in
            
            if error != nil{
                self.alert(title: "Error", message: "Dados incorretos, verifique e tente novamente")
                print("Dados incorretos, verifique e tente novamente")
            }else{
                if usuario == nil {
                    self.alert(title: "Problem", message: "Tivemos um problema inesperado")
                    print("Tivemos um problema inesperado")
                }else{
                    self.alert(title: "Parabéns", message: "Login feito com sucesso!!!")
                    print("Login feito com sucesso!!!")
                }
            }
        })
        
        // DEPOIS Q TERMINAR, USAR ISSO PRA VER SE ESTA OK NA CAPTURA DO TEXTFIELD
            
        //print("Os dados do usuário: email: \(email) password: \(password)" )
        
        
    }
    
    @IBAction func clickSignUpButton(_ sender: UIButton) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.emailTexteField {
            self.passwordTextField.becomeFirstResponder()
        }else {
            self.passwordTextField.resignFirstResponder()
        }
        return true
    }
    
    
}
