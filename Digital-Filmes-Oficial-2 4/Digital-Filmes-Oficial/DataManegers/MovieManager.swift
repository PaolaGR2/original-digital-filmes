//
//  MovieManager.swift
//  Digital-Filmes-Oficial
//
//  Created by Mariana Eri Massaki on 10/11/21.
//

import Foundation

struct MovieManager {
    enum Section: String, CaseIterable {
        case HIGHLIGHTS = "Highlights"
        case PREVIEW = "Preview"
        case POPULAR = "Popular"
        case ANIME = "Anime"
        case CLASSIC = "Classic"
        case DOCUMENTARIES = "Documentaries"
        case DRAMAS = "Dramas"
        case MUSIC = "Music"
    }
    
    static var movies = [
        Section.HIGHLIGHTS: [
            Movie(title: "Station 19", headerImage: , description: "Watch movie"),
            Movie(title: "Michelle Wolf", headerImage: , description: "Watch season 3"),
            Movie(title: "Know Down", headerImage: , description: "Watch season 1")
        ],
        Section.PREVIEW: [
            Movie(title: "Ant Man", thumbnail: <#T##UIImage?#>),
            Movie(title: "Avatar", thumbnail: <#T##UIImage?#>),
            Movie("Hobbit", thumbnail: <#T##UIImage?#>),
            Movie(title: "How to train your dragon", thumbnail: <#T##UIImage?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>)
        ],
        Section.POPULAR: [
            Movie(title: "Ant Man", thumbnail: <#T##UIImage?#>),
            Movie(title: "Avatar", thumbnail: <#T##UIImage?#>),
            Movie("Hobbit", thumbnail: <#T##UIImage?#>),
            Movie(title: "How to train your dragon", thumbnail: <#T##UIImage?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>)
        ],
        Section.ANIME: [
            Movie(title: "Ant Man", thumbnail: <#T##UIImage?#>),
            Movie(title: "Avatar", thumbnail: <#T##UIImage?#>),
            Movie("Hobbit", thumbnail: <#T##UIImage?#>),
            Movie(title: "How to train your dragon", thumbnail: <#T##UIImage?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>)
        ],
        Section.CLASSIC: [
            Movie(title: "Ant Man", thumbnail: <#T##UIImage?#>),
            Movie(title: "Avatar", thumbnail: <#T##UIImage?#>),
            Movie("Hobbit", thumbnail: <#T##UIImage?#>),
            Movie(title: "How to train your dragon", thumbnail: <#T##UIImage?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>)
        ],
        Section.DOCUMENTARIES: [
            Movie(title: "Ant Man", thumbnail: <#T##UIImage?#>),
            Movie(title: "Avatar", thumbnail: <#T##UIImage?#>),
            Movie("Hobbit", thumbnail: <#T##UIImage?#>),
            Movie(title: "How to train your dragon", thumbnail: <#T##UIImage?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>)
        ],
        Section.DRAMAS: [
            Movie(title: "Ant Man", thumbnail: <#T##UIImage?#>),
            Movie(title: "Avatar", thumbnail: <#T##UIImage?#>),
            Movie("Hobbit", thumbnail: <#T##UIImage?#>),
            Movie(title: "How to train your dragon", thumbnail: <#T##UIImage?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>)
        ],
        Section.MUSIC: [
            Movie(title: "Ant Man", thumbnail: <#T##UIImage?#>),
            Movie(title: "Avatar", thumbnail: <#T##UIImage?#>),
            Movie("Hobbit", thumbnail: <#T##UIImage?#>),
            Movie(title: "How to train your dragon", thumbnail: <#T##UIImage?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>),
            Movie(title: "Harry Potter", description: <#T##String?#>),
            Movie(title: "Fridge Night", description: <#T##String?#>),
            Movie(title: "Moonlight", description: <#T##String?#>),
            Movie(title: "How to get away with murder", description: <#T##String?#>)
        ]
    ]
    
    
    
    
    
    
    
    
}
