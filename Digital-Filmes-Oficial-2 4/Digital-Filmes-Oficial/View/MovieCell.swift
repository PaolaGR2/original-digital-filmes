//
//  MovieCell.swift
//  Digital-Filmes-Oficial
//
//  Created by Paola Geremia on 09/11/21.
//

import UIKit

protocol MovieCell: UICollectionViewCell {
    
    func showMovie(movie: Movie?)
    
}
