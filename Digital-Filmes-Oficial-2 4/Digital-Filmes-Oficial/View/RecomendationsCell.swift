//
//  RecomendationsCell.swift
//  Digital-Filmes-Oficial
//
//  Created by Paola Geremia on 09/11/21.
//

import UIKit

class RecomendationsCell: UICollectionViewCell, MovieCell {
    
    static let reuseIdentifier = String(describing: RecomendationsCell.self)
    
    @IBOutlet weak var movieImage: UIImageView!
    
    func showMovie(movie: Movie?) {
        movieImage.image = movie?.thumbnail
    }
    
}
